package ru.rencredit.akaramyshev.mailsender.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import ru.rencredit.akaramyshev.mailsender.api.repository.ContactsRepository;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;
import ru.rencredit.akaramyshev.mailsender.mapper.ContactMapper;


import javax.sql.DataSource;
import java.util.List;

@Repository
public class ContactsRepositoryImpl extends JdbcDaoSupport implements ContactsRepository {

    @Autowired
    public ContactsRepositoryImpl(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    @Override
    public int insert(Contact contact) {
        logger.info("IDDDDDDD +++++++ " + contact.getId());
        return getJdbcTemplate().update(
                "insert into contacts( id, name, last_name, email) VALUES (?, ?, ?, ? )",
                contact.getId(),
                contact.getName(),
                contact.getLastName(),
                contact.getEmail()
        );
    }

    @Override
    public int update(Contact contact) {
        return getJdbcTemplate().update(
                "update contacts set email = ? where id = ?",
                contact.getEmail(), contact.getId()
        );
    }

    @Override
    public int removeById(String id) {
        return getJdbcTemplate().update(
                "delete contacts where id = ?", id
        );
    }

    @Override
    public List<Contact> findAll() {
        return getJdbcTemplate().query(
                "select * from contacts",
                new ContactMapper()
        );
    }

}
