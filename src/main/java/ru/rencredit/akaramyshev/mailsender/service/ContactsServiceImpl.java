package ru.rencredit.akaramyshev.mailsender.service;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rencredit.akaramyshev.mailsender.api.repository.ContactsRepository;
import ru.rencredit.akaramyshev.mailsender.api.service.ContactsService;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactsServiceImpl implements ContactsService {

    private static final Logger log = LoggerFactory.getLogger(ContactsServiceImpl.class);

    @Autowired
    private ContactsRepository contactsRepository;

    @Override
    @SneakyThrows
    public void add(Contact contact) {
        if (contact == null) throw new Exception();
        contactsRepository.insert(contact);
    }

    @Override
    @SneakyThrows
    public List<Contact> findAll() {
        List<Contact> contactList = contactsRepository.findAll();
        if (contactList.isEmpty() || contactList == null) throw new Exception();
        return contactList;
    }

    @Override
    @SneakyThrows
    public void update(Contact contact) {
        if (contact == null) throw new Exception();
        contactsRepository.update(contact);
    }

    @Override
    @SneakyThrows
    public void removeById(String id) {
        if (id.isEmpty() || id == null) throw new Exception();
        contactsRepository.removeById(id);
    }

    @Override
    public List<String> findAllEmail() {
        List<String> contacts = new ArrayList<>();
        for (Contact contact : findAll()) {
            contacts.add(contact.getEmail());
        }
        return contacts;
    }

}
