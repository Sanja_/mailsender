package ru.rencredit.akaramyshev.mailsender.service;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.rencredit.akaramyshev.mailsender.api.service.EmailService;

import java.util.List;


@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public SimpleMailMessage prepareMessage(String... email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Перерывчик");
        message.setText("Pss, PChelovek, не хочешь попить чайку?");

        return message;
    }

    @Override
    @SneakyThrows
    public String send(List<String> email) {
        if (email.isEmpty()) throw new Exception();
        String[] mal = email.toArray(new String[email.size()]);
        this.emailSender.send(prepareMessage(mal));
        return "Email Successful";
    }

}
