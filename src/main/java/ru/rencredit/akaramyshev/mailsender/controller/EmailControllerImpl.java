package ru.rencredit.akaramyshev.mailsender.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import ru.rencredit.akaramyshev.mailsender.api.controller.ContactController;
import ru.rencredit.akaramyshev.mailsender.api.controller.EmailController;
import ru.rencredit.akaramyshev.mailsender.api.service.ContactsService;
import ru.rencredit.akaramyshev.mailsender.api.service.EmailService;


@Controller
public class EmailControllerImpl implements EmailController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private ContactController contactController;

    @Autowired
    private ContactsService contactsService;

    private static final Logger log = LoggerFactory.getLogger(EmailControllerImpl.class);

    @Override
    @Scheduled(cron = "0 0 17 ? * MON-FRI")
    public void sendEmail() {
        log.info(emailService.send(contactsService.findAllEmail()));
        contactController.outListContacts();
    }

}