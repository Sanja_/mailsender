package ru.rencredit.akaramyshev.mailsender.controller;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.akaramyshev.mailsender.api.controller.ContactController;
import ru.rencredit.akaramyshev.mailsender.api.service.ContactsService;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.util.List;

@Controller
@NoArgsConstructor
public class ContactControllerImpl implements ContactController {

    private static final Logger log = LoggerFactory.getLogger(ContactControllerImpl.class);

    @Autowired
    private ContactsService contactsService;

    @Override
    @SneakyThrows
    public void outListContacts() {
        List<String> contactList = contactsService.findAllEmail();

        if (contactList.isEmpty()) {
            log.info("LIST CONTACTS ISEMPTY ");
            throw new Exception();
        }
    }

    @Override
    public void add(Contact contacts) {
        contactsService.add(contacts);
    }

    @Override
    public void update(Contact contact) {
        contactsService.update(contact);
    }

    @Override
    public List<Contact> findAll() {
       return contactsService.findAll();
    }

    @Override
    public void removeById(String id) {
        contactsService.removeById(id);
    }

}
