package ru.rencredit.akaramyshev.mailsender.api.controller;

import org.springframework.scheduling.annotation.Scheduled;

public interface EmailController {

    @Scheduled(cron = "0 0 17 ? * MON-FRI")
    void sendEmail();

}
