package ru.rencredit.akaramyshev.mailsender.api.repository;

import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.util.List;

public interface ContactsRepository {

    int insert(Contact contact);

    int update(Contact contact);

    int removeById(String id);

    List<Contact> findAll();

}
