package ru.rencredit.akaramyshev.mailsender.api.service;

import lombok.SneakyThrows;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.util.List;

public interface ContactsService {

    @SneakyThrows
    void add(Contact contact);

    @SneakyThrows
    List<Contact> findAll();

    @SneakyThrows
    void update(Contact contact);

    @SneakyThrows
    void removeById(String id);

    List<String> findAllEmail();

}
