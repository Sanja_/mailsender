package ru.rencredit.akaramyshev.mailsender.api.controller;

import lombok.SneakyThrows;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.util.List;

public interface ContactController {

    @SneakyThrows
    void outListContacts();

    void add(Contact contacts);

    void update(Contact contact);

    List<Contact> findAll();

    void removeById(String id);

}
