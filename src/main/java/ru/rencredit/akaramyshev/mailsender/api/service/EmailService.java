package ru.rencredit.akaramyshev.mailsender.api.service;

import lombok.SneakyThrows;
import org.springframework.mail.SimpleMailMessage;

import java.util.List;

public interface EmailService {
    SimpleMailMessage prepareMessage(String... email);

    @SneakyThrows
    String send(List<String> email);
}
