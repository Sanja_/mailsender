package ru.rencredit.akaramyshev.mailsender.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.rencredit.akaramyshev.mailsender.entity.Contact;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ContactMapper implements RowMapper<Contact> {

    @Override
    public Contact mapRow(ResultSet resultSet, int i) throws SQLException {
        Contact contact = new Contact();
        contact.setId(resultSet.getString("id"));
        contact.setName(resultSet.getString("name"));
        contact.setLastName(resultSet.getString("last_name"));
        contact.setEmail(resultSet.getString("email"));
        return contact;
    }

}
